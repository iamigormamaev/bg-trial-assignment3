public class Main {
    public static void main(String[] args) {
        try {
            Config.applicationInit(args);
        } catch (IllegalArgumentException e) {
            System.out.println("Для запуска программы передайте в первом аргументе ключ для подключения к API translate.yandex.ru");
            e.printStackTrace();
            System.exit(1);
        }
        Implementation implementation = new Implementation();
        implementation.translate();
    }
}
