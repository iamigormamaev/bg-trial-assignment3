public class Config {
    private static String YANDEX_API_TRANSLATE_KEY;

    public static String getYANDEX_API_TRANSLATE_KEY() {
        return YANDEX_API_TRANSLATE_KEY;
    }

    public static void applicationInit(String[] args) {
        try {
            YANDEX_API_TRANSLATE_KEY = args[0];
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
