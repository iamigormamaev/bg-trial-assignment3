import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Implementation {

    public void translate() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Введите фразу на английском языке - она будет переведена на русский.\nДля выхода введите exit.");
            String s = br.readLine();
            while (!s.equals("exit")) {
                System.out.println(getTranslateFromYandex(s));
                s = br.readLine();
            }
            br.close();
        } catch (IOException e) {
            System.out.println("Ошибка ввода. Завершение программы.");
            System.exit(1);
        }

    }

    private String getTranslateFromYandex(String s) {
        try {
            HttpResponse<JsonNode> jsonResponse = Unirest.post("https://translate.yandex.net/api/v1.5/tr.json/translate")
                    .queryString("key", Config.getYANDEX_API_TRANSLATE_KEY())
                    .field("text", s)
                    .field("lang", "en-ru")
                    .asJson();

            switch (jsonResponse.getStatus()) {
                case 422:
                    return "Текст не может быть переведен";
                case 200:
                    return ((JSONArray) jsonResponse.getBody().getObject().get("text")).getString(0);
                default:
                    return "Произошла ошибка при вызове сервиса translate.yandex.ru";
            }

        } catch (UnirestException e) {
            return "Произошла ошибка при вызове сервиса translate.yandex.ru";
        }
    }
}
